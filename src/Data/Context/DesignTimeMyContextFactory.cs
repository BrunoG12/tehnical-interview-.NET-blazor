﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace Data.Context
{
    [Obsolete("Should only be used for generating EF migrations.", true)]
    public class DesignTimeMyContextFactory : IDesignTimeDbContextFactory<MyContext>
    {
        public MyContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<MyContext>();
            optionsBuilder.UseSqlite($"Data Source={nameof(MyContext)}.db");

            return new MyContext(optionsBuilder.Options);
        }
    }
}
