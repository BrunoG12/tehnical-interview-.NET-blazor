﻿using Web.Features.Billing.Abstractions;
using Web.Features.Billing.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Web.Features.Billing.Services
{
    public sealed class BillingService : IBillingService
    {
        /// <inheritdoc/>
        public Task<bool> HasAccessAsync(BillingContext MyContext, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public Task<int> CalculatePriceAsync(BillingContext MyContext, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
